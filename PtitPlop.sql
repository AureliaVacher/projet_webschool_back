-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: May 12, 2019 at 04:42 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `Planneo`
--

-- --------------------------------------------------------

--
-- Table structure for table `budget`
--

CREATE TABLE `budget` (
  `id_budget` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `id_budget_category` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `currency` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `budget`
--

INSERT INTO `budget` (`id_budget`, `id_event`, `id_budget_category`, `name`, `amount`, `currency`) VALUES
(3, 1, NULL, 'fleurs', 700, '₪'),
(5, 1, NULL, 'ballon', 1000, '₪'),
(6, 1, NULL, ' sdsdsds', 2, '$'),
(7, 1, NULL, ' sdsdsds', 2, '$'),
(8, 1, NULL, ' sdsdsds', 2, '$'),
(9, 1, NULL, ' plop', 7000, '₪');

-- --------------------------------------------------------

--
-- Table structure for table `business_user`
--

CREATE TABLE `business_user` (
  `id_business_user` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id_contact` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id_contact`, `name`, `email`, `phone`) VALUES
(1, 'Aurelia coontact', 'contact@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id_event` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_contact` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `in_progress` tinyint(1) DEFAULT NULL,
  `public?` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id_event`, `id_user`, `id_contact`, `name`, `description`, `create_time`, `start_time`, `end_time`, `in_progress`, `public?`) VALUES
(1, 1, 1, 'Premier event', 'Ceci est la description de mon premier event', '2019-04-16 18:05:35', '2019-05-15 00:00:00', '2019-04-30 00:00:00', NULL, NULL),
(2, 1, 1, 'Deuxième event', 'description du deuxième evenement', '2019-04-16 18:15:49', '2019-07-10 00:00:00', '2019-04-27 00:00:00', 1, NULL),
(3, 1, 1, 'Troisième event', 'description troisième event', '2019-04-10 21:00:00', '2019-04-18 00:00:00', '2019-04-30 00:00:00', NULL, NULL),
(4, 1, NULL, ' blop', ' piou piou', '2019-05-12 11:17:58', '2019-05-10 03:33:00', '2019-05-11 03:33:00', NULL, NULL),
(5, 1, NULL, ' trdfgdssdfsd', ' fdsfdsfdsf', '2019-05-12 11:26:45', '2019-05-10 22:02:00', '2019-05-11 22:02:00', NULL, NULL),
(6, 1, NULL, ' trdfgdssdfsd', ' fdsfdsfdsf', '2019-05-12 11:26:45', '2019-05-10 22:02:00', '2019-05-11 22:02:00', NULL, NULL),
(7, 1, NULL, ' trdfgdssdfsd', ' fdsfdsfdsf', '2019-05-12 11:29:16', '2019-05-10 22:02:00', '2019-05-11 22:02:00', NULL, NULL),
(8, 1, NULL, ' trdfgdssdfsd', ' fdsfdsfdsf', '2019-05-12 11:29:44', '2019-05-10 22:02:00', '2019-05-11 22:02:00', NULL, NULL),
(9, 1, NULL, ' trdfgdssdfsd', ' fdsfdsfdsf', '2019-05-12 11:30:28', '2019-05-10 22:02:00', '2019-05-11 22:02:00', NULL, NULL),
(10, 1, NULL, ' vdsfdsqfd', ' qsfsdqfsdfsq', '2019-05-12 11:31:01', '2019-05-16 22:02:00', '2019-05-17 22:02:00', NULL, NULL),
(11, 1, NULL, ' rdfsdfsd', 'sdsfsdf ', '2019-05-12 11:32:17', '2019-05-24 22:02:00', '2019-05-29 22:02:00', NULL, NULL),
(12, 1, NULL, ' sdqsdqs', 'dqsdqsd ', '2019-05-12 11:35:09', '2019-05-25 22:02:00', '2019-05-26 22:02:00', NULL, NULL),
(13, 1, NULL, ' fsdfsfsd', ' fsdfsdf', '2019-05-12 11:36:08', '2019-05-23 03:03:00', '2019-05-24 03:03:00', NULL, NULL),
(14, 1, NULL, ' fsdfsdfsdf', ' fdsfsdfsd', '2019-05-12 12:04:14', '2019-05-08 22:02:00', '2019-05-08 22:02:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event_category`
--

CREATE TABLE `event_category` (
  `id_event` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `id_guest` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `id_guest_category` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `mail` varchar(45) NOT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `adress` varchar(45) DEFAULT NULL,
  `response` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `guest_category`
--

CREATE TABLE `guest_category` (
  `id_guest_category` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `name_category` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE `place` (
  `id_place` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `adress` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `place`
--

INSERT INTO `place` (`id_place`, `id_event`, `name`, `description`, `adress`, `city`, `picture`) VALUES
(1, 1, 'Place de mon premier event', 'Ceci est la description de la place de mon premier event', '10 bd JB Romeuf', '63130 ROYAT', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `regular_user`
--

CREATE TABLE `regular_user` (
  `id_regular_user` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regular_user`
--

INSERT INTO `regular_user` (`id_regular_user`, `id_user`, `firstname`, `lastname`) VALUES
(1, 1, 'Aurelia', 'Vacher'),
(2, 2, 'dsfsdfsdf', 'sfsdfdsf'),
(3, 3, 'aurelia', 'vacher'),
(4, 4, 'oreo', 'plop'),
(5, 5, 'firebase', 'aur&eacute;'),
(6, 6, '', ''),
(7, 14, 'ila', 'oreo');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id_task` int(11) NOT NULL,
  `id_task_category` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `details` varchar(255) DEFAULT NULL,
  `limite_time` datetime DEFAULT NULL,
  `done` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id_task`, `id_task_category`, `name`, `details`, `limite_time`, `done`) VALUES
(1, 0, 'Premiere tache', 'Description premiere tache', '2019-04-25 00:00:00', NULL),
(2, 0, 'Deuxième tache', 'Description de la deuxième tache', '2019-04-30 00:00:00', NULL),
(3, 1, 'piou piou', 'plopidou', NULL, NULL),
(4, 1, 'pioupiou piou piou', 'details de piou piou', NULL, 0),
(5, 1, 'pioupiou piou piou', 'details de piou piou', NULL, 0),
(6, 1, '', '', NULL, 0),
(7, 1, '', '', NULL, 0),
(8, 1, 'nouvelle tache', ' nouvelle tache', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_category`
--

CREATE TABLE `task_category` (
  `id_task_category` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_category`
--

INSERT INTO `task_category` (`id_task_category`, `id_event`, `name`) VALUES
(1, 1, 'blob'),
(2, 1, 'jesapellegroot'),
(13, 1, 'essai ajout category'),
(14, 1, ' dernier essai');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id_transaction` int(11) NOT NULL,
  `id_budget` int(11) NOT NULL,
  `cost` tinyint(1) NOT NULL,
  `amount` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `details` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id_transaction`, `id_budget`, `cost`, `amount`, `name`, `details`) VALUES
(1, 3, 1, 25, 'rose', 'plop rose'),
(2, 3, 1, 4565, 'plop', 'gfhjkgjfhgj'),
(3, 5, 1, 2345, 'dfghjk', 'cfgvhbjk'),
(4, 5, 1, 5678, ' dfsdfsd', ' fdsfsd'),
(5, 5, 1, 45678, ' dsfdsd', ' dfdsfs'),
(6, 5, 1, 45678, ' dsfdsd', ' dfdsfs'),
(7, 5, 1, 456789, ' dfssdfdsf', ' dfdsfds'),
(8, 5, 1, 456789, ' dfssdfdsffdfdffdfdf', ' dfdsfds'),
(9, 5, 1, 456789, ' dsfdsfsdf', ' dsfdsfsdf'),
(10, 5, 1, 34567890, ' ESSAI', ' ESSAI'),
(11, 6, 1, 34567890, ' ESSAI', ' ESSAI'),
(12, 6, 1, 34567890, ' ESSAI', ' ESSAI'),
(13, 6, 1, 34567890, ' ESSAIdfghjk', ' ESSAI'),
(14, 3, 0, 78690, ' essai sa mere', ' dfsfdsfdsf'),
(15, 3, 1, 4356789, ' plopidou', ' sfdsfdsf'),
(16, 3, 0, 567890, ' dfsfdsf', ' sdsfdsf'),
(17, 3, 1, 45678, 'depense', ' dfdsfsdfd'),
(18, 9, 1, 22323, ' sdsqdqsd', ' rezrzrezr');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `id_firebase` varchar(255) NOT NULL,
  `business` tinyint(1) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_firebase`, `business`, `email`, `password`, `notes`, `picture`, `create_time`, `last_login`, `active`) VALUES
(1, '21C66TrepYT7LwAewn2FERpVIQ52', NULL, 'aurelia.h.vacher@gmail.com', 'AA2411apm', 'plop', NULL, '2019-03-25 12:26:44', '2019-04-21 13:57:56', NULL),
(2, '', NULL, 'sfdsfs@sdfsdf', 'sdfsdfdsfdsf', 'sdfsfs', NULL, '2019-03-31 20:46:03', '2019-03-31 20:46:03', NULL),
(3, '', NULL, 'aurelia.h.vacher@gmail.com', 'AA2411apmapmapm', 'dssdsfdsf', NULL, '2019-03-31 20:51:26', '2019-03-31 20:51:26', NULL),
(4, '', NULL, 'oreo@gmail.com', 'AA2411apm', '', NULL, '2019-04-16 16:49:24', '2019-04-16 16:49:24', NULL),
(5, 'H3V7gwGkPmMiIJrg1VNzBft0q6i1', NULL, 'firebase@check.com', 'AA2411apm', '', NULL, '2019-04-16 20:43:57', '2019-04-16 20:43:57', NULL),
(14, 'jq4LdNjwhXNCiieE1QaVvCtY2xn1', NULL, 'artemis@gmail.com', 'fghjkjkhlkjh', '', NULL, '2019-04-30 15:29:15', '2019-04-30 15:29:15', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `budget`
--
ALTER TABLE `budget`
  ADD PRIMARY KEY (`id_budget`),
  ADD KEY `id_event_idx` (`id_event`),
  ADD KEY `id_budget_category_idx` (`id_budget_category`);

--
-- Indexes for table `business_user`
--
ALTER TABLE `business_user`
  ADD PRIMARY KEY (`id_business_user`),
  ADD UNIQUE KEY `id_user_UNIQUE` (`id_user`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id_event`),
  ADD KEY `id_user_idx` (`id_user`),
  ADD KEY `id_contact_idx` (`id_contact`);

--
-- Indexes for table `event_category`
--
ALTER TABLE `event_category`
  ADD KEY `id_event_idx` (`id_event`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`id_guest`),
  ADD KEY `id_guest_category_idx` (`id_guest_category`),
  ADD KEY `id_event_idx` (`id_event`);

--
-- Indexes for table `guest_category`
--
ALTER TABLE `guest_category`
  ADD PRIMARY KEY (`id_guest_category`),
  ADD KEY `id_event_idx` (`id_event`);

--
-- Indexes for table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`id_place`),
  ADD KEY `id_event_idx` (`id_event`);

--
-- Indexes for table `regular_user`
--
ALTER TABLE `regular_user`
  ADD PRIMARY KEY (`id_regular_user`),
  ADD UNIQUE KEY `id_user_UNIQUE` (`id_user`) USING BTREE;

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id_task`),
  ADD KEY `id_task_category_idx` (`id_task_category`);

--
-- Indexes for table `task_category`
--
ALTER TABLE `task_category`
  ADD PRIMARY KEY (`id_task_category`) USING BTREE,
  ADD KEY `id_event_idx` (`id_event`) USING BTREE;

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id_transaction`),
  ADD KEY `id_budget_idx` (`id_budget`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `budget`
--
ALTER TABLE `budget`
  MODIFY `id_budget` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `business_user`
--
ALTER TABLE `business_user`
  MODIFY `id_business_user` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id_contact` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `event_category`
--
ALTER TABLE `event_category`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `id_guest` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guest_category`
--
ALTER TABLE `guest_category`
  MODIFY `id_guest_category` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `place`
--
ALTER TABLE `place`
  MODIFY `id_place` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `regular_user`
--
ALTER TABLE `regular_user`
  MODIFY `id_regular_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `id_task` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `task_category`
--
ALTER TABLE `task_category`
  MODIFY `id_task_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id_transaction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `budget`
--
ALTER TABLE `budget`
  ADD CONSTRAINT `id_budget_category` FOREIGN KEY (`id_budget_category`) REFERENCES `budget_category` (`id_budget_category`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `id_event7` FOREIGN KEY (`id_event`) REFERENCES `event` (`id_event`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `business_user`
--
ALTER TABLE `business_user`
  ADD CONSTRAINT `id_user2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `id_contact` FOREIGN KEY (`id_contact`) REFERENCES `contact` (`id_contact`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `id_user3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event_category`
--
ALTER TABLE `event_category`
  ADD CONSTRAINT `id_event` FOREIGN KEY (`id_event`) REFERENCES `event` (`id_event`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `guest`
--
ALTER TABLE `guest`
  ADD CONSTRAINT `id_event5` FOREIGN KEY (`id_event`) REFERENCES `event` (`id_event`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `id_guest_category` FOREIGN KEY (`id_guest_category`) REFERENCES `guest_category` (`id_guest_category`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `guest_category`
--
ALTER TABLE `guest_category`
  ADD CONSTRAINT `id_event4` FOREIGN KEY (`id_event`) REFERENCES `event` (`id_event`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `place`
--
ALTER TABLE `place`
  ADD CONSTRAINT `id_event3` FOREIGN KEY (`id_event`) REFERENCES `event` (`id_event`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `regular_user`
--
ALTER TABLE `regular_user`
  ADD CONSTRAINT `id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `id_task_category_idx` FOREIGN KEY (`id_task_category`) REFERENCES `task_category` (`id_task_category`);

--
-- Constraints for table `task_category`
--
ALTER TABLE `task_category`
  ADD CONSTRAINT `id_event_task` FOREIGN KEY (`id_event`) REFERENCES `event` (`id_event`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `id_budget` FOREIGN KEY (`id_budget`) REFERENCES `budget` (`id_budget`) ON DELETE NO ACTION ON UPDATE NO ACTION;