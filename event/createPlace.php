<?php 
    header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Allow-Headers: Content-Type, Authorization");
    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE); // données récupérés de l'input

    //connexion à la bdd 
    function  connexion(){
        $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
        ini_set('display_errors', 1);
        $erreur = "";
        return $bdd;
    }
    //function modèle pour insertion donnée
    function insererdonnee($tableau,$tableau_c,$table){
        //$tableau : nouvelles données
        //$tableau_c : colonnes dans lesquelles mettrent les nouvelels données
        //$tablea : table de bdd correspondant
        $bdd =  connexion(); //appelle à la fonction connectant la bdd
        $i = 0;
        $b = "";
        $c = "";
        while($i<count ($tableau)){
            $tableau[$i] = "'".htmlentities($tableau[$i])."'"; 
            $b = $b.$tableau[$i];
            $c = $c.$tableau_c[$i];
            $i++;
            if($i ==count ($tableau)){
                break;
            }else{
                $b = $b.",";
                $c = $c.",";
            }
        }
        //echo "INSERT INTO ".$table." ($c) VALUES ($b)"; // essai pour afficher l'intérieur du prepare
        $reponse1 = $bdd->prepare("INSERT INTO ".$table." ($c) VALUES ($b)"); //requete SQL
        $donnes1 = $reponse1->execute(); // insère la ligne
    }
   
        $tableau_place= array($obj['placeName'],$obj['placeAdress'], $obj['placeCity'], $obj['placeDescription'], $obj['id_event']);  
        $tableau_place_c = array('name',"adress", "city", "description", "id_event");
        insererdonnee($tableau_place, $tableau_place_c, "place");
    
    echo json_encode($obj, JSON_FORCE_OBJECT);
?>