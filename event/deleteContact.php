<?php 

    header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Allow-Headers: Content-Type, Authorization");
    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE);

    //connexion à la bdd 
    function connexion(){
        $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
        ini_set('display_errors', 1);
        $erreur = "";
        return $bdd; 
    }

    function delete($table,$attribut, $value){
        $bdd = connexion();
        $query= $bdd->query("DELETE FROM " . $table . " WHERE " . $attribut . "='" . $value . "'");
        return $query;
    }

    delete("contact", "id_contact", $obj);
    echo json_encode($obj, JSON_FORCE_OBJECT);

?>