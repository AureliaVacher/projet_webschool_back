<?php 
    header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Allow-Headers: Content-Type, Authorization");
    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE);

    //connexion à la bdd 
    function connexion(){
        $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
        ini_set('display_errors', 1);
        $erreur = "";
        return $bdd; 
    }

    function delete($table,$attribut, $value){
        $bdd = connexion();
        $query = $bdd->query("DELETE FROM " . $table . " WHERE " . $attribut . "='" . $value . "'");
        return $query;
    }

    function select_several($colonne,$table,$apreswhere,$attribut){
        $bdd =  connexion();
        $reponse = $bdd->query("SELECT " .$colonne . " FROM " . $table . " WHERE " . $apreswhere . "='" . $attribut . "'");
        $array = [];
        while ($donne = $reponse->fetch_assoc()) { 
            array_push($array, [$colonne=>$donne[$colonne]]);
        }
        return $array;
    }
    
    $id_event = $obj;
    // 1 - supression Contact
    delete("contact", "id_event", $id_event);
    // 2 - suppression Place
    delete("place", "id_event", $id_event);
    // 3 - Tasks 
    $categories = select_several('id_task_category','task_category','id_event',$id_event);
    for($i=0; $i<count($categories); $i++){
        delete("task", "id_task_category", $categories[$i]['id_task_category']);
    };
    // 4 - supression category
    delete("task_category", "id_event", $id_event);
    // 5 - suprression transactions
    $budgets = select_several('id_budget','budget','id_event',$id_event);
    for($j=0; $j<count($budgets); $j++){
        delete("transaction", "id_budget", $budgets[$j]['id_budget']);
    };
    // 6 - suppression budget
    delete("budget", "id_event", $id_event);
    // 7-  suppresion event
    //delete("event", "id_event", $id_event);

    echo json_encode($obj, JSON_FORCE_OBJECT);
?>