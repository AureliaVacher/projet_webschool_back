<?php 
  header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Allow-Headers: Content-Type, Authorization");
  $json = file_get_contents('php://input');
  $obj = json_decode($json, TRUE); // données récupérés de l'input

  //connexion à la bdd 
  function connexion(){
      $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
      ini_set('display_errors', 1);
      $erreur = "";
      return $bdd;
  }
  
  //function modèle pour insertion donnée
  function insererdonnee($tableau,$tableau_c,$table){
    //$tableau : nouvelles données
    //$tableau_c : colonnes dans lesquelles mettrent les nouvelels données
    //$tablea : table de bdd correspondant
    $bdd = connexion(); //appelle à la fonction connectant la bdd
    $i = 0;
    $b = "";
    $c = "";
    while($i<count ($tableau)){
        $tableau[$i] = "'".htmlentities($tableau[$i])."'"; 
        $b = $b.$tableau[$i];
        $c = $c.$tableau_c[$i];
        $i++;
        if($i ==count ($tableau)){
            break;
        }else{
            $b = $b.",";
            $c = $c.",";
        }
    }
    //echo "INSERT INTO ".$table." ($c) VALUES ($b)"; // essai pour afficher l'intérieur du prepare
    $reponse1 = $bdd->prepare("INSERT INTO ".$table." ($c) VALUES ($b)"); //requete SQL
    $donnes1 = $reponse1->execute(); // insère la ligne
  }

  function select_particulier($colonne,$table,$apreswhere,$atribut){
    $bdd = connexion();
    $reponse = $bdd->query("SELECT " .$colonne . " FROM " . $table . " WHERE " . $apreswhere . "='" . $atribut . "'");
    $donne = $reponse->fetch_array();
    return $donne;
  }
  
  $tableau = array($obj["id_firebase"], $obj["email"], $obj["password"], $obj["notes"]);  
  $tableau_c = array("id_firebase", "email", "password", "notes");

  $tableau_regular = array($obj["firstname"], $obj["lastname"]);
  $tableau_regular_c = array("firstname", "lastname");

  $tableau_business = array($obj["username"]);
  $tableau_business_c = array("username");

  if($obj){
    insererdonnee($tableau, $tableau_c, "user"); 
    $id_fk = select_particulier("id_user", "user", "email", $obj["email"]);// recupere ID user

    if($obj["statusUser"]) {
      //---------- business user ---------------//
      array_push($tableau_business, $id_fk[0]);
      array_push($tableau_business_c, "id_user");
      insererdonnee($tableau_business, $tableau_business_c, "business_user");
    } else {
      //---------- regular user ---------------//
      array_push($tableau_regular, $id_fk[0]);
      array_push($tableau_regular_c, "id_user");
      insererdonnee($tableau_regular, $tableau_regular_c, "regular_user");
    }
  }

  echo json_encode($obj, JSON_FORCE_OBJECT);
?>