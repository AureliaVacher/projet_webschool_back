<?php 
    header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Allow-Headers: Content-Type, Authorization");
    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE);

    //connexion à la bdd 
    function connexion(){
        $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
        ini_set('display_errors', 1);
        $erreur = "";
        return $bdd; 
    }
    
    function select_elements($colonne,$colonne2,$colonne3,$colonne4,$table,$apreswhere,$attribut){
        $bdd = connexion();
        $reponse = $bdd->query("SELECT " .$colonne . ", " . $colonne2 . ", " . $colonne3 . ", " . $colonne4 . " FROM " . $table . " WHERE " . $apreswhere . "='" . $attribut . "'");
        $array = [];
        while ($donne = $reponse->fetch_assoc()) { 
            array_push($array, [$colonne=>$donne[$colonne], $colonne2=>$donne[$colonne2], $colonne3=>$donne[$colonne3], $colonne4=>$donne[$colonne4]]);
        }
        return $array;
    }

    function select_transactions($colonne,$colonne2,$colonne3,$colonne4,$colonne5, $colonne6,$colonne7, $table,$apreswhere,$attribut){
        $bdd = connexion();
        $reponse = $bdd->query("SELECT " .$colonne . ", " . $colonne2 . ", " . $colonne3 . ", " . $colonne4 .  ", " . $colonne5 .", " . $colonne6 . ", " . $colonne7 ." FROM " . $table . " WHERE " . $apreswhere . "='" . $attribut . "'");
        $array = [];
        while ($donne = $reponse->fetch_assoc()) { 
            array_push($array, [$colonne=>$donne[$colonne], $colonne2=>$donne[$colonne2], $colonne3=>$donne[$colonne3], $colonne4=>$donne[$colonne4], $colonne5=>$donne[$colonne5], $colonne6=>$donne[$colonne6],$colonne7=>$donne[$colonne7]]);
        }
        return $array;
    }


    
    $allBudgets = select_elements("id_budget","name","amount","currency","budget","id_event", $obj);
    $countBudgets = sizeof($allBudgets);
    $budgetsToDisplay = [] ;
    
    for ($j=0; $j< $countBudgets; $j++) {
        array_push($budgetsToDisplay, $allBudgets[$j]);
        $allTransactions = select_transactions('id_transaction',"name","amount","cost","details","createTransaction","date_transaction", "transaction", "id_budget", $allBudgets[$j]['id_budget']);
        array_push($budgetsToDisplay[$j], $allTransactions); 
    }
    
    echo json_encode($budgetsToDisplay, JSON_FORCE_OBJECT);
?>