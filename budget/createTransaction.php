<?php 

  header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Allow-Headers: Content-Type, Authorization");
  $json = file_get_contents('php://input');
  $obj = json_decode($json, TRUE); // données récupérés de l'input

  //connexion à la bdd 
  function  connexion(){
      $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
      ini_set('display_errors', 1);
      $erreur = "";
      return $bdd;
  }
  //function modèle pour insertion donnée
  function insererdonnee($tableau,$tableau_c,$table){
    //$tableau : nouvelles données
    //$tableau_c : colonnes dans lesquelles mettrent les nouvelels données
    //$tablea : table de bdd correspondant
    $bdd =  connexion(); //appelle à la fonction connectant la bdd
    $i = 0;
    $b = "";
    $c = "";
    while($i<count ($tableau)){
        $tableau[$i] = "'".htmlentities($tableau[$i])."'"; 
        $b = $b.$tableau[$i];
        $c = $c.$tableau_c[$i];
        $i++;
        if($i ==count ($tableau)){
            break;
        }else{
            $b = $b.",";
            $c = $c.",";
        }
    }
    //echo "INSERT INTO ".$table." ($c) VALUES ($b)"; // essai pour afficher l'intérieur du prepare
    $reponse1 = $bdd->prepare("INSERT INTO ".$table." ($c) VALUES ($b)"); //requete SQL
    $donnes1 = $reponse1->execute(); // insère la ligne
  }

  function select_particulier($colonne,$table,$apreswhere,$atribut){
    $bdd =  connexion();
    $reponse = $bdd->query("SELECT " .$colonne . " FROM " . $table . " WHERE " . $apreswhere . "='" . $atribut . "'");
    $donne = $reponse->fetch_array();
    return $donne;
  }
  
  //premier cas, si la catégorie est deja crée
  if($obj["newSpendingCategory"]){
    $idBudget = $obj["newSpendingCategory"];
    $tableau_transaction = array($obj["newSpendingType"], $obj["newSpendingAmount"] , $obj["newSpendingName"], $obj["newSpendingDescription"], $obj["dateTransaction"], $idBudget);  
    $tableau_transaction_c = array("cost", "amount", "name", "details","date_transaction", "id_budget");
    insererdonnee($tableau_transaction, $tableau_transaction_c, "transaction");
  }

  //deuxieme cas, si la catégorie doit être crée
  else if ($obj["newCategoryName"]){
    $tableau_budget = array($obj["id_event"], $obj["newCategoryName"] , $obj["newCategoryAmount"], $obj["newCategoryCurrency"]);  
    $tableau_budget_c = array("id_event", "name", "amount", "currency");
    insererdonnee($tableau_budget, $tableau_budget_c, "budget");
    $idBudget = select_particulier('id_budget','budget','name', $obj["newCategoryName"]);   
      if($idBudget){
        $tableau_transaction1 = array($obj["newSpendingType"], $obj["newSpendingAmount"] , $obj["newSpendingName"], $obj["newSpendingDescription"], $obj["dateTransaction"], $idBudget['id_budget']);  
        $tableau_transaction1_c = array("cost", "amount", "name", "details","date_transaction", "id_budget");
        insererdonnee($tableau_transaction1, $tableau_transaction1_c, "transaction");
      }
  }


  echo json_encode($obj, JSON_FORCE_OBJECT);
?>