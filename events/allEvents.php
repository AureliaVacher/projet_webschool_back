<?php 
    header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Allow-Headers: Content-Type, Authorization");
    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE);

    //connexion à la bdd 
    function  connexion(){
        $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
        ini_set('display_errors', 1);
        $erreur = "";
        return $bdd; 
    }

    function select_particulier($colonne,$table,$apreswhere,$atribut){
        $bdd =  connexion();
        $reponse = $bdd->query("SELECT " .$colonne . " FROM " . $table . " WHERE " . $apreswhere . "='" . $atribut . "'");
        $donne = $reponse->fetch_array();
        return $donne;
    }
    
    function select_events($colonne,$colonne2,$colonne3,$colonne4,$table,$apreswhere,$attribut){
        $bdd =  connexion();
        $reponse = $bdd->query("SELECT " .$colonne . ", " . $colonne2 . ", " . $colonne3 . ", " . $colonne4 . " FROM " . $table . " WHERE " . $apreswhere . "='" . $attribut . "'");
        $array = [];
        while ($donne = $reponse->fetch_assoc()) { 
            array_push($array, [$colonne=>$donne[$colonne], $colonne2=>$donne[$colonne2], $colonne3=>$donne[$colonne3], $colonne4=>$donne[$colonne4]]);
        }
        return $array;
    }

    $idUser = select_particulier("id_user", "user", "id_firebase", $obj);
    $allEvents = select_events("name","description","start_time","id_event","event", "id_user", $idUser["id_user"]);

    echo json_encode($allEvents, JSON_FORCE_OBJECT);
?>