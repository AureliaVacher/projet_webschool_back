<?php 

  header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Allow-Headers: Content-Type, Authorization");
  $json = file_get_contents('php://input');
  $obj = json_decode($json, TRUE);

  //connexion à la bdd 
  function important(){
    $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
    ini_set('display_errors', 1);
    $erreur = "";
    return $bdd;
  }

  // // function 1 : selectionner donnée BDD 
  // function select_particulier($colonne,$table,$apreswhere,$atribut){
  //   $bdd = important();
  //   $reponse = $bdd->query("SELECT " .$colonne . " FROM " . $table . " WHERE " . $apreswhere . "='" . $atribut . "'");
  //   $donne = $reponse->fetch_array();
  //   return $donne;
  // }

  // function 2 : update
  function update_tables($tableau,$tableau_c,$table,$apreswhere, $attribut){
    //$tableau : nouvelles données
    //$tableau_c : colonnes dans lesquelles mettrent les nouvelels données
    //$table : table de bdd correspondant
    $bdd = important();
    $i = 0;
    $b = "";
    $c = "";
    while($i<count ($tableau)){
      $tableau[$i] = "'".htmlentities($tableau[$i])."'"; 
      $b = $b.$tableau[$i];
      $c = $c.$tableau_c[$i];
      $i++;
      if($i ==count ($tableau)){
          break;
      }else{
          $b = $b.",";
          $c = $c.",";
      }
    }
    $reponse = $bdd->prepare("UPDATE " .$table . " SET ($c) VALUES ($b) WHERE " . $apreswhere . "='" . $attribut . "'");
    $donne = $reponse->execute();
    return $donne;
  }

  // 1 - recupere valeur de basse dans la bdd
  // $initialEmail = select_particulier("email","user","id_user", 1); 
  // $initialNotes = select_particulier("notes","user","id_user", 1); //verif echo json_encode($obj[1], JSON_FORCE_OBJECT);

  // $tableau = array($obj[0], $obj[1]);  
  $tableau = array($obj["email"], $obj["notes"]);  
  $tableau_c = array("email", "notes");

  // $sameValue = array("input = initial");
  // $diferentValues =  array("input ≠ initial");
  // $inputNull =  array("Pas de new input");

  if($obj){
    update_tables($tableau, $tableau_c, "user", "id_user", 1); 
  }
  

  //echo json_encode($obj, JSON_FORCE_OBJECT);

  // 2 - compare à valeur de l'input / Si valeur différente - > update
  // $sameValue = array("input = initial");
  // $diferentValues =  array("input ≠ initial");
  // $inputNull =  array("Pas de new input");
  
  // // echo json_encode($obj, JSON_FORCE_OBJECT); //valeur de l'input
  // // echo json_encode($initialEmail[0], JSON_FORCE_OBJECT); //valeur initial
  
  // if($obj){
  //   if($initialEmail[0] == $obj[0] && $initialNotes[0] == $obj[1]){
  //     echo json_encode($sameValue);
  //   } else {
  //     update_tables("user","email",$obj[0],"id_user", 1);
      
  //     update_tables("user","notes",$obj[1],"id_user", 1);
  //     echo json_encode($diferentValues);
  //   }
  // } else {
  //   echo json_encode($inputNull);
  // }
 
?>


