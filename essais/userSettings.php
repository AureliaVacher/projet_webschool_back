<?php 

  header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Allow-Headers: Content-Type, Authorization");
  $json = file_get_contents('php://input');
  $obj = json_decode($json, TRUE);

  //connexion à la bdd 
  function important(){
    $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
    ini_set('display_errors', 1);
    $erreur = "";
    return $bdd;
  }

  // function 1 : selectionner donnée BDD 
  function select_particulier($colonne,$table,$apreswhere,$atribut){
    $bdd = important();
    $reponse = $bdd->query("SELECT " .$colonne . " FROM " . $table . " WHERE " . $apreswhere . "='" . $atribut . "'");
    $donne = $reponse->fetch_array();
    return $donne;
  }

  // function 2 : update
  function update_tables($table,$innitialColumn,$newValue,$id,$atribut){
    $bdd = important();
    $reponse = $bdd->query("UPDATE " .$table . " SET " . $innitialColumn . " = '" . $newValue . "' WHERE " . $id . "=" . $atribut . "");
    $donne = $reponse->fetch_array();
    return $donne;
  }

  // 1 - recupere valeur de basse dans la bdd
  $initialEmail = select_particulier("email","user","id_user", 1); //verif echo json_encode($prevEmail, JSON_FORCE_OBJECT) ok

  // 2.0 - trouver valeur input ok -> a voir avec plusieur inputs
  
  // 2 - compare à valeur de l'input / Si valeur différente - > update
  $sameValue = array("input = initial");
  $diferentValues =  array("input ≠ initial");
  $inputNull =  array("Pas de new input");
  //echo json_encode($obj, JSON_FORCE_OBJECT); //valeur de l'input
  //echo json_encode($initialEmail[0], JSON_FORCE_OBJECT); //valeur initial
  if($obj){
    if($initialEmail[0] == $obj){
      echo json_encode($sameValue);
    } else {
      update_tables("user","email",$obj,"id_user", 1);
      echo json_encode($diferentValues);
    }
  } else {
    echo json_encode($inputNull);
  }
 
?>
