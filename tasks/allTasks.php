<?php 
    header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Allow-Headers: Content-Type, Authorization");
    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE); // données récupérés de l'input

    //connexion à la bdd 
    function connexion(){
        $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
        ini_set('display_errors', 1);
        $erreur = "";
        return $bdd; 
    }
    
    function select_category($colonne,$colonne2,$table,$apreswhere,$attribut){
        $bdd = connexion();
        $reponse = $bdd->query("SELECT " .$colonne . ", " . $colonne2 . " FROM " . $table . " WHERE " . $apreswhere . "='" . $attribut . "'");
        $array = [];
        while ($donne = $reponse->fetch_assoc()) { 
            array_push($array, [$colonne=>$donne[$colonne], $colonne2=>$donne[$colonne2]]);
        }
        return $array;
    }
    function select_task($colonne,$colonne2,$colonne3,$colonne4,$table,$apreswhere,$attribut){
        $bdd = connexion();
        $reponse = $bdd->query("SELECT " .$colonne . ", " . $colonne2 .  ", " . $colonne3 .  ", " . $colonne4 . " FROM " . $table . " WHERE " . $apreswhere . "='" . $attribut . "'");
        $array = [];
        while ($donne = $reponse->fetch_assoc()) { 
            array_push($array, [$colonne=>$donne[$colonne], $colonne2=>$donne[$colonne2], $colonne3=>$donne[$colonne3], $colonne4=>$donne[$colonne4]]);
        }
        return $array;
    }

    
    $allTaskCategory = select_category("id_task_category","name","task_category","id_event", $obj);
    $countCategory = sizeof($allTaskCategory);
    $taskCategoryToDisplay = [] ;
    
    for ($j=0; $j< $countCategory; $j++) {
        array_push($taskCategoryToDisplay, $allTaskCategory[$j]);
        $allTasks = select_task("id_task","name","details","done","task", "id_task_category", $allTaskCategory[$j]['id_task_category']);
        array_push($taskCategoryToDisplay[$j], $allTasks); 
    }
    echo json_encode($taskCategoryToDisplay, JSON_FORCE_OBJECT);
?>