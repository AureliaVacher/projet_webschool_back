<?php 
    header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Allow-Headers: Content-Type, Authorization");
    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE);

    //connexion à la bdd 
    function important(){
        $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
        ini_set('display_errors', 1);
        $erreur = "";
        return $bdd;
    }

    function join_tables($select,$table1,$table2,$id,$atribut){
        $bdd = important();
        $reponse = $bdd->query("SELECT " .$select . " FROM " . $table1 . " JOIN " . $table2 ." ON " . $table1 . "." . $id . "=" . $table2 . "." . $id . " WHERE " . $table1 . "." . $id . "=" . $atribut . "");
        $donne = $reponse->fetch_assoc();
        //fetch array crée un array en double
        return $donne;
    }

    $infoUser = json_encode(join_tables("*", "user", "regular_user", "id_user", 1), JSON_FORCE_OBJECT);
    echo $infoUser;
?>