<?php 
    header("Access-Control-Allow-Origin:  http://localhost:3000");//npm start
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Allow-Headers: Content-Type, Authorization");
    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE);

    //connexion à la bdd 
    function important(){
        $bdd = new mysqli('localhost', 'aurelia', '' , 'Planneo'); // dispo dans PHP My admin
        ini_set('display_errors', 1);
        $erreur = "";
        return $bdd;
    }
    //Fonction selectionner une donnée particuliere
    function select_particulier($colonne,$table,$apreswhere,$atribut){
        $bdd = important();
        $reponse = $bdd->query("SELECT " .$colonne . " FROM " . $table . " WHERE " . $apreswhere . "='" . $atribut . "'");
        $donne = $reponse->fetch_assoc();
        return $donne;
    }
    $idEvent = $obj;
    $infoEvent = ['event'=>select_particulier('*', 'event', 'id_event', $idEvent)];
    $infoPlace = ['place'=>select_particulier('*', 'place', 'id_event', $idEvent)];
    $infoBudget = ['budget'=>select_particulier('*', 'budget', 'id_event', $idEvent)];
    $infoContact = ['contact'=>select_particulier('*', 'contact', 'id_event', $idEvent)];

    $sumInfos = $infoEvent + $infoPlace+ $infoBudget+ $infoContact;
    echo json_encode($sumInfos, JSON_FORCE_OBJECT);
?>